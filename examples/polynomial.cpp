#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#define AFFINE_MULT 2

#include <qifen.h>
#include <iostream>
#include <boost/numeric/ublas/io.hpp>
#include <boost/scope_exit.hpp>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>
#include <cstdlib>
#include <chrono>

#include <qifen/internal.hpp>

template <typename F>
::std::chrono::nanoseconds time(F f)
{
	using namespace ::std::chrono;

	high_resolution_clock::time_point begin, end;

	begin = high_resolution_clock::now();
	f();
	end = high_resolution_clock::now();

	return duration_cast<nanoseconds>(end - begin);
}

::std::pair<double, double> calc_qifen()
{
	::qifen_qi_context_t ctx;
	::qifen_qi_t x, f, t;

	::qifen_qi_context_init(ctx);
	::qifen_qi_init_infsup(ctx, x, -15.0, -10.0);
	::qifen_qi_init(ctx, f);
	::qifen_qi_init(ctx, t);

	BOOST_SCOPE_EXIT_ALL(&) {
		::qifen_qi_clear(ctx, x);
		::qifen_qi_clear(ctx, f);
		::qifen_qi_clear(ctx, t);
		::qifen_qi_context_clear(ctx);
	};

	::qifen_qi_set_infsup_string(ctx, f, "0.6", nullptr);
	::qifen_qi_mul(ctx, f, f, x);
	::qifen_qi_add_scalar(ctx, f, f, 37.5);
	::qifen_qi_mul(ctx, f, f, x);
	::qifen_qi_add_scalar(ctx, f, f, 935);
	::qifen_qi_mul(ctx, f, f, x);
	::qifen_qi_add_scalar(ctx, f, f, 11625);
	::qifen_qi_mul(ctx, f, f, x);
	::qifen_qi_add_scalar(ctx, f, f, 72072);
	::qifen_qi_mul(ctx, f, f, x);

	::qifen_qi_set_infsup_string(ctx, t, "38.33", nullptr);
	::qifen_qi_add(ctx, f, f, t);

	return ::qifen_qi_get_infsup(ctx, f);
}

::std::pair<double, double> calc_kv(double a = 0.5, double b = 5.0)
{
	::kv::affine<double> x, f;

	x = ::kv::interval<double>(a, b);

	f = "0.6" * x;
	f += 37.5;
	f *= x;
	f += 935;
	f *= x;
	f += 11625;
	f *= x;
	f += 72972;
	f *= x;
	f += "38.33";

	auto i = to_interval(f);

	return ::std::make_pair(i.lower(), i.upper());
}

int main()
{
	::std::cout.setf(::std::ios::scientific);
	::std::cout.precision(15);

	auto q = ::calc_qifen();
	::std::cout << q.first << ' ' << q.second << ' ' << q.second - q.first << ' ';

	::std::size_t a = 0;

	for (::std::size_t i = 0; i < 10000; ++i) {
		auto t = ::time([&]() {
			calc_qifen();
		});
		a += t.count();
	}

	::std::cout << ::std::chrono::duration_cast<::std::chrono::microseconds>(::std::chrono::nanoseconds(a)).count() / 10000.0 << ::std::endl;

	q = ::calc_kv();
	::std::cout << q.first << ' ' << q.second << ' ' << q.second - q.first << ' ';

	a = 0;

	for (::std::size_t i = 0; i < 10000; ++i) {
		auto t = ::time([&]() {
			::kv::affine<double>::maxnum() = 0;
			calc_kv();
		});
		a += t.count();
	}

	::std::cout << ::std::chrono::duration_cast<::std::chrono::microseconds>(::std::chrono::nanoseconds(a)).count() / 10000.0 << ::std::endl;

	constexpr ::std::size_t n = 3;

	double inf = -178229.0, sup = -178229.0;

	auto t = ::time([&]() {
		for (::std::size_t i = 0; i < n; ++i) {
			double a = -15.0 + 5.0 * i / n;
			double b = -15.0 + 5.0 * (i + 1) / n;
			::kv::affine<double>::maxnum() = 0;

			auto r = calc_kv(a, b);

			inf = ::std::min(inf, r.first);
			sup = ::std::max(sup, r.second);
		}
	});

	::std::cout << inf << ' ' << sup << ' ' << sup - inf << ' ' << ::std::chrono::duration_cast<::std::chrono::microseconds>(t).count() << ::std::endl;
}
