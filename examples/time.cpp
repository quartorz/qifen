#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#include <chrono>
#include <iostream>
#include <qifen.h>
#include <kv/affine.hpp>
#include <kv/rdouble.hpp>

template <typename F>
auto time(F f)
{
	using namespace ::std::chrono;

	high_resolution_clock::time_point begin, end;

	begin = high_resolution_clock::now();
	f();
	end = high_resolution_clock::now();

	return end - begin;
}

int main()
{
	double inf = 1.5, sup = 2.0;

	auto test_qi = [&](const char *name, size_t n,  const ::qifen_qi_inv_config_struct &config) {
		::qifen_qi_context_t ctx;
		::qifen_qi_t x, y;

		::qifen_qi_context_init(ctx);
		::qifen_qi_init(ctx, x);
		::qifen_qi_init(ctx, y);

		auto t = ::std::chrono::duration_cast<::std::chrono::nanoseconds>(time([&]() {
			for (size_t i = 0; i < n; ++i) {
				ctx->num_dummy = 0;
				::qifen_qi_set_infsup(ctx, x, inf, sup);
				::qifen_qi_inv(ctx, y, x, config);
			}
		}));

		auto a = ::qifen_qi_get_infsup(ctx, y);

		::std::cout << "qifen_qi_t, " << name  << ", " << n << " loops" << ::std::endl;
		::std::cout << "  result: [" << a.first << ", " << a.second << "]" << ::std::endl;
		::std::cout << "  total time: " << t.count() << "ns" << ::std::endl;
		::std::cout << "  time per loop: " << long long(double(t.count()) / double(n)) << "ns" << ::std::endl;

		::qifen_qi_clear(ctx, x);
		::qifen_qi_clear(ctx, y);
		::qifen_qi_context_clear(ctx);
	};

	auto test_kv = [&](size_t n) {
		using affine_t = ::kv::affine<double>;

		affine_t x, y;

		auto t = ::std::chrono::duration_cast<::std::chrono::nanoseconds>(time([&]() {
			for (size_t i = 0; i < n; ++i) {
				affine_t::maxnum() = 0;
				x = ::kv::interval<double>(1.5, 2.0);
				y = inv(x);
			}
		}));

		auto a = to_interval(y);

		::std::cout << "kv::affine<double>, " << n << " loops" << ::std::endl;
		::std::cout << "  result: " << a << ::std::endl;
		::std::cout << "  total time: " << t.count() << "ns" << ::std::endl;
		::std::cout << "  time per loop: " << long long(double(t.count()) / double(n)) << "ns" << ::std::endl;
	};

	::std::cout.setf(::std::cout.fixed);
	::std::cout.precision(16);

	test_qi("method 1 (Chebyshev interpolation)", 10000, { ::qifen_approx_chebyshev });
	test_qi("method 2-1 (Remez method, 1 iteration)", 10000, {  ::qifen_approx_remez, 1});
	test_qi("method 2-2 (Remez method, 10 iterations, tol=1e-10)", 10000, { ::qifen_approx_remez, 10, 1e-10 });
	test_qi("method 2-2 (Remez method, 10 iterations, tol=1e-16)", 10000, { ::qifen_approx_remez, 10, 1e-16 });
	test_qi("method 2-2 (Remez method, 20 iterations, tol=1e-16)", 10000, { ::qifen_approx_remez, 20, 1e-16 });
	test_qi("method 3", 10000, { ::qifen_approx_fast });
	test_kv(10000);
}
