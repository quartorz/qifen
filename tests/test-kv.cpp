#define CATCH_CONFIG_MAIN
#include <catch.hpp>

#define QIFEN_CONFIG_INTERVAL_KV
#define QIFEN_CONFIG_DISABLE_INTLAB

#include <qifen.h>

TEST_CASE("qifen_interval_t", "[interval]") {
	::qifen_interval_t a, b;

	SECTION("initialize") {
		::qifen_interval_init(a);
		::qifen_interval_init_infsup(b, 0.0, 1.0);

		auto r = ::qifen_interval_get_infsup(a);

		REQUIRE(r.first == 0.0);
		REQUIRE(r.second == 0.0);

		r = ::qifen_interval_get_infsup(b);

		REQUIRE(r.first == 0.0);
		REQUIRE(r.second == 1.0);
	}

	::qifen_interval_clear(a);
	::qifen_interval_clear(b);
}

TEST_CASE("qifen_qi_t", "[qi]") {
	::qifen_qi_context_t ctx;
	::qifen_qi_t a, b;

	::qifen_qi_context_init(ctx);

	SECTION("initialize") {

	}
}
