#pragma once

#include <qifen/interval.h>
#include <qifen/mex-compatible.h>

#include <boost/scope_exit.hpp>

namespace qifen {
	namespace internal {
		::kv::interval<double> interval_to_kv(const ::qifen_interval_t);
		::mxArray *interval_to_intlab(const ::qifen_interval_t);
		void interval_set(::qifen_interval_t, const ::kv::interval<double> &);
		void interval_set(::qifen_interval_t, ::mxArray *);
	}
}

#define QIFEN_VAR_KV_INTERVAL(name, value) ::kv::interval<double> name = ::qifen::internal::interval_to_kv(value)
#define QIFEN_VAR_INTLAB_INTERVAL(name, value)\
   ::mxArray *name = ::qifen::internal::interval_to_intlab(value);\
   QIFEN_VAR_INTLAB_INTERVAL_RELEASE(name)

#if defined(QIFEN_CONFIG_INTERVAL_KV)
#  define QIFEN_VAR_INTLAB_INTERVAL_RELEASE(name) BOOST_SCOPE_EXIT_ALL(&name){ ::mxDestroyArray(name); }
#  define QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest) \
     ::mxArray *lhs; \
     BOOST_SCOPE_EXIT_ALL(&lhs, &dest){ ::qifen::internal::interval_set(dest, lhs); ::mxDestroyArray(lhs); }
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#  define QIFEN_VAR_INTLAB_INTERVAL_RELEASE QIFEN_NOOP
#  define QIFEN_MEX_LHS_AS_DEST_INTERVAL(lhs, dest)  \
     ::mxArray *lhs; \
     BOOST_SCOPE_EXIT_ALL(&lhs, &dest) { ::mxDestroyArray(dest->value); dest->value = lhs; }
#endif
