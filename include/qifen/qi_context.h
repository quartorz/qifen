#pragma once

#include <qifen/types.h>
#include <qifen/config.h>

#if defined(__cplusplus)
extern "C" {
#endif

qifen_qi_context_ptr qifen_qi_context_new();
void qifen_qi_context_delete(qifen_qi_context_t v);
void qifen_qi_context_init(qifen_qi_context_t v);
void qifen_qi_context_clear(qifen_qi_context_t v);
void qifen_qi_context_set_num_dummy(qifen_qi_context_t v, size_t n);

#if defined(__cplusplus)
}
#endif
