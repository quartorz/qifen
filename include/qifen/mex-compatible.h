#pragma once

#if defined(MATLAB_MEX_FILE)
#  include "mex.h"
#else
typedef void mxArray;

#  if defined(__cplusplus)
extern "C" {
#  endif

int mexPrintf(const char *message, ...);

int mexCallMATLAB(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[], const char *fcn_name);

int mexCallMATLABWithObject(int nlhs, mxArray *plhs[], int nrhs, mxArray *prhs[], const char *fcn_name);

typedef enum {
	mxREAL,
	mxCOMPLEX
} mxComplexity;

bool mxIsClass(const mxArray *pa, const char *name);
bool mxIsScalar(const mxArray *pa);
mxArray *mxCreateDoubleScalar(double value);
mxArray *mxCreateDoubleMatrix(size_t m, size_t n, mxComplexity flag);
mxArray *mxCreateString(const char *str);
mxArray *mxDuplicateArray(const mxArray *in);
void mxDestroyArray(mxArray *pa);
mxArray *mxGetField(const mxArray *pa, size_t i, const char *fieldname);
void mxSetField(mxArray *pa, size_t i, const char *fieldname, mxArray *value);

void *mxRealloc(void *ptr, size_t size);

size_t mxGetNumberOfDimensions(const mxArray *pa);
const size_t *mxGetDimensions(const mxArray *pa);

double mxGetScalar(const mxArray *pa);

double *mxGetPr(const mxArray *pa);

void mxSetPr(mxArray *pa, double  *pr);

size_t mxGetM(const mxArray *pa);
size_t mxGetN(const mxArray *pa);
void mxSetM(mxArray *pa, size_t m);
void mxSetN(mxArray *pa, size_t n);

#  if defined(__cplusplus)
}
#  endif
#endif
