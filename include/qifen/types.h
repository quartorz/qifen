#pragma once

#include <qifen/config.h>
#include <stdbool.h>

#if !defined(QIFEN_CONFIG_DISABLE_INTLAB)
#  include <qifen/mex-compatible.h>

typedef struct {
#  if defined(__cplusplus)
	using matrix_t = mxArray *;
#  endif
	mxArray *value;
} qifen_matrix_intlab_struct, qifen_matrix_intlab_t[1], *qifen_matrix_intlab_ptr;

typedef struct {
#  if defined(__cplusplus)
	using interval_t = mxArray *;
#  endif
	mxArray *value;
} qifen_interval_intlab_struct, qifen_interval_intlab_t[1], *qifen_interval_intlab_ptr;

typedef struct {
#  if defined(__cplusplus)
	using interval_t = mxArray *;
	using matrix_t = mxArray *;
#  endif
	mxArray *value;
} qifen_interval_matrix_intlab_struct,
qifen_interval_matrix_intlab_t[1],
*qifen_interval_matrix_intlab_ptr;
#endif

#if !defined(QIFEN_CONFIG_DISABLE_KV)
#  if defined(__cplusplus)
#    include <memory>
#    include <boost/numeric/ublas/matrix.hpp>
#    include <kv/interval.hpp>

typedef struct qifen_matrix_kv_struct {
	using matrix_t = ::boost::numeric::ublas::matrix<double>;
	::std::aligned_storage_t<sizeof(matrix_t), alignof(matrix_t)> storage;
	matrix_t &operator*()
	{
		return *reinterpret_cast<matrix_t*>(&storage);
	}
	const matrix_t &operator*() const
	{
		return *reinterpret_cast<const matrix_t*>(&storage);
	}
	matrix_t *operator->()
	{
		return reinterpret_cast<matrix_t*>(&storage);
	}
	const matrix_t *operator->() const
	{
		return reinterpret_cast<const matrix_t*>(&storage);
	}
} qifen_matrix_kv_t[1], *qifen_matrix_kv_ptr;

typedef struct qifen_interval_kv_struct {
	using interval_t = ::kv::interval<double>;
	::std::aligned_storage_t<sizeof(interval_t), alignof(interval_t)> storage;
	interval_t &operator*()
	{
		return *reinterpret_cast<interval_t*>(&storage);
	}
	const interval_t &operator*() const
	{
		return *reinterpret_cast<const interval_t*>(&storage);
	}
	interval_t *operator->()
	{
		return reinterpret_cast<interval_t*>(&storage);
	}
	const interval_t *operator->() const
	{
		return reinterpret_cast<const interval_t*>(&storage);
	}
} qifen_interval_kv_t[1], *qifen_interval_kv_ptr;

typedef struct qifen_interval_matrix_kv_struct {
	using interval_t = ::kv::interval<double>;
	using matrix_t = ::boost::numeric::ublas::matrix<interval_t>;
	std::aligned_storage_t<sizeof(matrix_t), alignof(matrix_t)> storage;
	matrix_t &operator*()
	{
		return *reinterpret_cast<matrix_t*>(&storage);
	}
	const matrix_t &operator*() const
	{
		return *reinterpret_cast<const matrix_t*>(&storage);
	}
	matrix_t *operator->()
	{
		return reinterpret_cast<matrix_t*>(&storage);
	}
	const matrix_t *operator->() const
	{
		return reinterpret_cast<const matrix_t*>(&storage);
	}
} qifen_interval_matrix_kv_t[1], *qifen_interval_matrix_kv_ptr;
#  else
#    error qifen_matrix_kv_struct cannot currently be used in C. use Intlab.
typedef void qifen_matrix_kv_struct;
typedef void *qifen_matrix_kv_t, *qifen_matrix_kv_ptr;
typedef void qifen_interval_kv_struct;
typedef void *qifen_interval_kv_t, *qifen_interval_kv_ptr;
typedef void qifen_interval_matrix_kv_struct;
typedef void *qifen_interval_matrix_kv_t, *qifen_interval_matrix_kv_ptr;
#  endif
#endif

#if defined(QIFEN_CONFIG_INTERVAL_KV)
typedef qifen_matrix_kv_struct qifen_matrix_struct;
typedef qifen_matrix_kv_t qifen_matrix_t;
typedef qifen_matrix_kv_ptr qifen_matrix_ptr;
typedef qifen_interval_kv_struct qifen_interval_struct;
typedef qifen_interval_kv_t qifen_interval_t;
typedef qifen_interval_kv_ptr qifen_interval_ptr;
typedef qifen_interval_matrix_kv_struct qifen_interval_matrix_struct;
typedef qifen_interval_matrix_kv_t qifen_interval_matrix_t;
typedef qifen_interval_matrix_kv_ptr qifen_interval_matrix_ptr;
#endif

#if defined(QIFEN_CONFIG_INTERVAL_INTLAB)
typedef qifen_matrix_intlab_struct qifen_matrix_struct;
typedef qifen_matrix_intlab_t qifen_matrix_t;
typedef qifen_matrix_intlab_ptr qifen_matrix_ptr;
typedef qifen_interval_intlab_struct qifen_interval_struct;
typedef qifen_interval_intlab_t qifen_interval_t;
typedef qifen_interval_intlab_ptr qifen_interval_ptr;
typedef qifen_interval_matrix_intlab_struct qifen_interval_matrix_struct;
typedef qifen_interval_matrix_intlab_t qifen_interval_matrix_t;
typedef qifen_interval_matrix_intlab_ptr qifen_interval_matrix_ptr;
#endif

typedef enum {
	qifen_verify_eig_auto,
	qifen_verify_eig_library_dependent,     // library-dependent method
	qifen_verify_eig_gershgorin,            // Gershgorin circle theorem
	qifen_verify_eig_jacobi_gershgorin,     // Jacobi method and Gershgorin circle theorem
} qifen_verify_eig_method_t;

typedef enum {
	qifen_approx_best_effort,   // Remez => Chebyshev => test
	qifen_approx_linear,        // linear approximation
	qifen_approx_chebyshev,     // Chebyshev interpolation
	qifen_approx_chebyshev_2,   // Chebyshev interpolation 2
	qifen_approx_remez,         // Remez algorithm
	qifen_approx_fast,
} qifen_approx_method_t;

typedef enum {
	qifen_error_succeeded,
	qifen_error_domain,
	qifen_error_eig,
	qifen_error_matrix_size,
	qifen_error_approx_failed,
} qifen_error_t;

typedef struct {
	size_t num_dummy;
} qifen_qi_context_struct, qifen_qi_context_t[1], *qifen_qi_context_ptr;

typedef struct {
	double x0;
	qifen_matrix_t a;
	qifen_matrix_t A;
	double delta;       // >= 0
	double quad_min;
	double quad_max;
	bool is_quad_zero, has_quad_minmax;
} qifen_qi_struct, qifen_qi_t[1], *qifen_qi_ptr;

typedef struct {
	qifen_approx_method_t approx_method;
	size_t num_iter;        // works only if approx_method is qifen_approx_remez or qifen_approx_best_effort
	double tolerance;	    // works only if approx_method is qifen_approx_remez or qifen_approx_best_effort
} qifen_qi_inv_config_struct, qifen_qi_inv_config_t[1], *qifen_qi_inv_config_ptr;
