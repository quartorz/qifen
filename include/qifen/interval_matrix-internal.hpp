#pragma once

#include <qifen/interval_matrix.h>
#include <qifen/mex-compatible.h>

#include <boost/scope_exit.hpp>

namespace qifen {
	namespace internal {
#if defined(QIFEN_CONFIG_INTERVAL_KV)
		const ::boost::numeric::ublas::matrix<::kv::interval<double>> &interval_matrix_to_kv(const ::qifen_interval_matrix_t);
		::boost::numeric::ublas::matrix<::kv::interval<double>> &interval_matrix_to_kv(::qifen_interval_matrix_t);
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
		::boost::numeric::ublas::matrix<::kv::interval<double>> interval_matrix_to_kv(const ::qifen_interval_matrix_t);
#endif
		::mxArray *interval_matrix_to_intlab(const ::qifen_interval_matrix_t);
		void interval_matrix_set(::qifen_interval_matrix_t, const ::boost::numeric::ublas::matrix<::kv::interval<double>> &);
		void interval_matrix_set(::qifen_interval_matrix_t, ::mxArray *);
	}
}

#define QIFEN_VAR_INTLAB_INTERVAL_MATRIX(name, value) \
   ::mxArray *name = ::qifen::internal::interval_matrix_to_intlab(value); \
   QIFEN_VAR_INTLAB_INTERVAL_MATRIX_RELEASE(name)

#if defined(QIFEN_CONFIG_INTERVAL_KV)
#  define QIFEN_VAR_KV_INTERVAL_MATRIX(name, value) \
     auto &name = ::qifen::internal::interval_matrix_to_kv(value)
#  define QIFEN_VAR_INTLAB_INTERVAL_MATRIX_RELEASE(name) BOOST_SCOPE_EXIT_ALL(&name){ ::mxDestroyArray(name); }
#  define QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest) \
     ::mxArray *lhs; \
     BOOST_SCOPE_EXIT_ALL(&lhs, &dest){ ::qifen::internal::interval_matrix_set(dest, lhs); ::mxDestroyArray(lhs); }
#elif defined(QIFEN_CONFIG_INTERVAL_INTLAB)
#  define QIFEN_VAR_KV_INTERVAL_MATRIX(name, value) \
     auto name = ::qifen::internal::interval_matrix_to_kv(value)
#  define QIFEN_VAR_INTLAB_INTERVAL_MATRIX_RELEASE QIFEN_NOOP
#  define QIFEN_MEX_LHS_AS_DEST_INTERVAL_MATRIX(lhs, dest) \
     ::mxArray *lhs; \
     BOOST_SCOPE_EXIT_ALL(&lhs, &dest) { ::mxDestroyArray(dest->value); dest->value = lhs; }
#endif
