#pragma once
#include <utility>

#include <qifen/config.h>
#include <qifen/types.h>
#include <qifen/interval.h>
#include <qifen/matrix.h>

#include <kv/interval.hpp>

namespace qifen {
	namespace internal {
		::std::pair<double, double> verify_linpart_range(qifen_qi_context_t ctx, const qifen_matrix_t a);
		::std::pair<double, double> verify_quadpart_range(qifen_qi_context_t ctx, const qifen_matrix_t a);
	}
}
